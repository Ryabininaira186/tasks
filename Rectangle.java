public class Rectangle extends Figure{
    public String fName;
    public int fLength;
    public int fWidth;
    public Rectangle(double x, double y, String figure, int length, int width) {
        super(x, y);
        fName = figure;
        fLength = length;
        fWidth = width;
    }
    @Override
    public void square() {
        int square = fLength * fWidth;
        System.out.println(fName + "- Площадь этого прямоугольника равна: " + square);
        //System.out.println("Длина прямоугольника равна: " + fLength); // для проверки площади
        //System.out.println("Ширина прямоугольника равна: " + fWidth); // для проверки площади
    }
}
