public abstract class FifthMain {
    public static void main(String[] args) {
        Figure[] myFigure = {
                new Circle(-5, 5, "Первый круг ", (int) (Math.random() * 11)),
                new Rectangle(10, -10, "Первый прямоугольник ", (int) (Math.random() * 21), (int) (Math.random() * 21)),
                new Rectangle(-20, -20, "Второй прямоугольник ", (int) (Math.random() * 21), (int) (Math.random() * 21)),
                new Circle(4, 3, "Второй круг ", (int) (Math.random() * 11)),
                new Circle(-2,0, "Третий круг ", (int) (Math.random() * 11))};
        for (int i = 0; i <= 4; i++){
            myFigure[i].square();
            myFigure[i].getQuadrant();
        }
    }
}
