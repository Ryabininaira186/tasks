public class FourthMain {
    // Напишите метод, заменяющий в произвольной строке все вхождения слова «бяка» на «вырезано цензурой».

    public static void main(String[] args) {
        String s1 = "Отдайте дяде эту бяку.";
        System.out.println("Исходное предложение: " + s1);
        stringMethod(s1);
    }

    private static void stringMethod(String s1) {
        String s2 = s1.replaceAll("бяку", "вырезано цензурой");
        System.out.println("Предложение изменено на: " + s2);
    }
}
