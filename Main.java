package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Введите математическую операцию: ");
        String operation = in.nextLine();
        System.out.print("Введите первую переменную: ");
        double variable1 = in.nextInt();
        System.out.print("Введите вторую переменную: ");
        double variable2 = in.nextInt();

        double result = 0;
        String error = " ";
        switch (operation) {
            case "+":
                result = variable1 + variable2;
                break;
            case "-":
                result = variable1 - variable2;
                break;
            case "*":
                result = variable1 * variable2;
                break;
            case "/":
                result = variable1 / variable2;
                break;
            default:
                error = "Неверный символ";
                System.out.println(error);
                break;
        }
        System.out.println("Ответ: " + result);
    }
}