import java.util.Scanner;
public class ThirdMain {
//Напишите программу, которая увеличивает элементы одномерного массива на 10%, а затем сортирует эти элементы методом «пузырька» по убыванию.
// Выведите результат данных операций в консоль.

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите элементы массива: ");
        double[] arr = new double[5];
        for (int i = 0; i < arr.length; i++){
            double number = in.nextDouble();
            arr[i] = number + 0.1 * number;
        }

        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < 4; j++) {
                if (arr[j] < arr[j + 1]) {
                    double bubble = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = bubble;
                }
            }
        }

        System.out.print("Изменённый массив: ");
        for (double result : arr) {
            System.out.print(result + ", ");
        }
    }
}
